################################## << TODO >>  ########################################
#	1. Сделать формирование отчета работы в виде html файла
#	2. Отправить отчет админу на почту
#	3. Проверка на выполнение внутри скрипта. Если например tar не отработал, сообщать
#		на почту админу
#	4. Загрузка новых бекапов на VPS  через rsync (зеркалирование архивов)
#######################################################################################

#!/bin/bash

backup_dir="/mnt/misc/backup"
LOG_POSTGRES="base_all_postgres.txt"

DIR_POSTGRES="/mnt/misc/backup/postgres_dump"

##################################
PG_DUMP="/usr/bin/pg_dump"
TAR="/bin/tar"
PSQL="/usr/bin/psql"

#Переключатели
val_dir="0"
val_postgres="1"

cd $backup_dir

if [ $val_dir -eq 1 ]; then

    echo "Собираем в один архив важные данные (/etc, /var/opt/drwcsd, /var/log, /var/www, /opt/drwcs"

    # Бэкап всего что нужно
    tar -cvvzf $backup_dir/backup-server-`date '+%m_%d_%Y'`.tar.bz2 \
    /etc/ \
    /var/opt/drwcs/ \
    /var/www/ \
    /var/log/ \
    /opt/drwcs/ \
    --exclude=$backup_dir > ./last.log
    else
        echo "Бекап каталогов пропущен"
fi

if [ $val_postgres -eq 1 ]; then
    cd $DIR_POSTGRES
    echo "DUMP Postgres base all"
#    psql -U postgres -l |  awk '{ print $1 }' | grep -v "-" | grep -v "|" |sed '3,18!d' > $LOG_POSTGRES
    $PSQL -U postgres -l |  awk '{ print $1 }' | grep -v -E "\-|\|" |sed '3,18!d' > $LOG_POSTGRES
    cat $LOG_POSTGRES | while read line
    do
        echo -e "######## Делаем дамп базы ########## $line"
#       $PG_DUMP -h localhost -U postgres -Fc -Z9 -c -f `date +"${line}.backups-%m-%d-%Y.dump"` $line
        $PG_DUMP -h localhost -U postgres -Fc $line > `date +"${line}.backups-%m-%d-%Y.dump"`
    done
    echo "############ Запаковываем все это добро в один архив #############"
    $TAR -czvf $backup_dir/postgres-`date '+%m_%d_%Y'`.tar.bz2 /mnt/misc/backup/postgres_dump/
    else
        echo "Postgres пропущен"
fi

cd $backup_dir
#Стираем файлы бэкапа старше 30 дней
find . -mtime +30 -exec rm '{}' \;
#Стираем старые логи
#find /var/log/ -type f -name *\.gz -exec rm '{}' \;
