#!/bin/bash

CAM="1"
TIME_VIDEO="31"

DIR_VIDEO="/home/cloud-user/video/"
date=`date '+%d-%m-%Y'`


#mkdir ${DIR_VIDEO}$date
#echo $date

# Снимаем видео поток с первой IP камкры
cvlc  --rtsp-tcp rtsp://gb2:gb2@194.190.30.156:554/cam/realmonitor?channel=$CAM \
                --sout="#std{access=file,mux=mp4,dst=${DIR_VIDEO}CAM1/CAM$CAM-$date.avi}" \
                --run-time=$TIME_VIDEO \
                --play-and-exit

cd $DIR_VIDEO/CAM1/
mencoder -oac copy -ovc copy *.avi -o $DIR_VIDEO/CAM1_ALL.avi
#Удаляем вчерашний конечный файл
rm $DIR_VIDEO/cam1_output.avi

# Увеличиваем FPS для ускорения видео
ffmpeg -r 96 -i $DIR_VIDEO/CAM1_ALL.avi -qscale 1 $DIR_VIDEO/cam1_output.avi